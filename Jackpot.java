//Safin Haque 2132492
public class Jackpot{
	public static void main(String[]args){
	
		Die die1=new Die();
		
		die1.roll();
		
		Board board=new Board();
		boolean gameOver=false;
		int numOfTilesClosed=0;
		
		int tries=0;
		
		System.out.println("Welcome to JackPot!");
	
		while(gameOver==false) //Runs the game
		{
			System.out.println(board);
			
			if(board.playATurn()==true)
			{
				gameOver=true;
				break;
			}
			else
			{
				numOfTilesClosed++;	
			}
			
		}
		
		System.out.println("Total closed " + numOfTilesClosed); //Tells you how many tiles have been closed
		
		
		if(numOfTilesClosed>=7)
		{
			System.out.println("You reached the Jackpot and Won! Congrats!");
		}
		else
		{
			System.out.println("Sorry, better luck next time!");
		}
	}
}