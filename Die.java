//Safin Haque 2132492
import java.util.Random;

public class Die{
	private int faceValue;
	private Random roll;
	
	
	
	public Die(){
		this.faceValue=1;
		this.roll= new Random();
	}
	
	public int getFaceValue(){
		return this.faceValue;
	}
	
	public void roll(){
		this.faceValue=roll.nextInt(6)+1;
	}
	
	public String toString(){
		return "The dice landed in " + this.faceValue;
	}
}